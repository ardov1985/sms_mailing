from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from server import views

router = DefaultRouter()
excluded = ('APIView', 'ViewSet', 'TokenObtainPairView', 'GenericAPIView')
for name in views.__dict__:
    if name.endswith('View') and name not in excluded:
        url = name.replace('View', '').lower()
        router.register(url, getattr(views, name), basename=url)


urlpatterns = [
    path('', include(router.urls)),
    path('token_obtain/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh_token/', TokenRefreshView.as_view(), name='token_refresh')
]

