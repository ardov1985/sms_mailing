import os

from celery.app import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sms_mailing.settings')

app = Celery('quick_publisher')
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()


app.conf.beat_schedule = {
    'send-report-every-single-minute': {
        'task': 'server.tasks.start_mailing',
        'schedule': crontab(),
    },
}
