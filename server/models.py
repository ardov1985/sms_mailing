from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db.models import *


class UserManager(BaseUserManager):
    def create_user(self, username: str, **kwargs) -> 'User':
        user = self.model(username=username, **kwargs)
        user.save(using=self._db)
        return user

    def create_superuser(self, username: str, password: str) -> 'User':
        user = self.create_user(username, is_staff=True, is_superuser=True)
        user.set_password(password)
        user.save()
        return user

    def create_staffuser(self, username: str, ) -> 'User':
        return self.create_user(username, is_staff=True)


class User(AbstractUser):
    """Класс авторизации администратора для управления рассылкой"""

    class StatusChoice(TextChoices):
        ACTIVE = 'active'
        BANNED = 'banned'

    objects = UserManager()

    username = CharField(max_length=64, db_index=True, unique=True)
    email = None
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []
    status = CharField(max_length=6, default=StatusChoice.ACTIVE, choices=StatusChoice.choices)

    class Meta:
        db_table = 'users'


class Operator(Model):
    """Наименование мобильного оператора"""
    name = CharField(max_length=16, unique=True)

    class Meta:
        db_table = 'operators'


class Tag(Model):
    """Тег пользователя"""
    name = CharField(max_length=32, unique=True)

    class Meta:
        db_table = 'tags'


class Mailing(Model):
    """Задание на рассылку"""

    class StatusChoice(TextChoices):
        """Статусы рассылки"""
        CREATED = 'created'  # создано
        MAILING = 'mailing'  # в процессе, рассылается
        COMPLETED = 'completed'  # завершено

    start = DateTimeField()  # дата и время запуска рассылки
    stop = DateTimeField()  # дата и время окончания рассылки
    text = CharField(max_length=255)  # текст рассылки
    status = CharField(max_length=19, choices=StatusChoice.choices, default=StatusChoice.CREATED)  # статус рассылки
    send_after = TimeField(blank=True, null=True)  # время после которого можно отправлять сообщения
    send_until = TimeField(blank=True, null=True)  # время до которого можно отправлять сообщения
    operators = ManyToManyField(Operator, blank=True, related_name='mailings')
    tags = ManyToManyField(Tag, blank=True, related_name='mailings')

    class Meta:
        db_table = 'mailings'


class Client(Model):
    """Цель рассылки"""
    operator = ForeignKey(Operator, on_delete=SET_NULL, null=True, db_index=True, related_name='clients')
    phone = CharField(max_length=11, unique=True)
    tag = ForeignKey(Tag, on_delete=SET_NULL, null=True, blank=True, db_index=True)
    timezone = CharField(max_length=32)

    class Meta:
        db_table = 'clients'


class Message(Model):
    """Рассылаемое сообщение"""

    class StatusChoice(TextChoices):
        """Статусы отправки сообщения"""
        CREATED = 'created'
        ERROR = 'error'
        SENT = 'sent'

    class Meta:
        db_table = 'messages'
        unique_together = ('mailing', 'client')

    created_at = DateTimeField(auto_now_add=True)
    status = CharField(max_length=8, choices=StatusChoice.choices, default=StatusChoice.CREATED)
    mailing = ForeignKey(Mailing, on_delete=CASCADE, related_name='messages')
    client = ForeignKey(Client, on_delete=CASCADE, related_name='clients')
