from rest_framework.permissions import BasePermission

from server.models import User


class IsActiveUser(BasePermission):
    """Предоставляет доступ только для активного пользователя"""

    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        return request.user.status == User.StatusChoice.ACTIVE

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated and request.user.status == User.StatusChoice.ACTIVE
