import pytz
from rest_framework.generics import GenericAPIView
from typing import Literal
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ViewSet, GenericViewSet
from .permissions import IsActiveUser
from .serializers import *


def create_view(model_name):
    """Метакласс создающий стандартные представления"""
    serializer = globals()[model_name + 'Serializer']
    model = globals()[model_name]
    return type(
        model_name,
        (ModelViewSet,),
        {'queryset': model.objects.all(), 'serializer_class': serializer, 'permission_classes': (IsActiveUser,)}
    )


SPECIFICATION_METHOD = Literal['add', 'remove']


class MailingsView(create_view('Mailing')):
    """Набор методов управления рассылкой"""
    @staticmethod
    def get_related_object(data, related_model):
        if 'id' in data:
            try:
                return related_model.objects.get(id=data['id']), None
            except related_model.DoesNotExist:
                return None, f'{related_model} does not exists'
        elif 'name' in data:
            try:
                return related_model.objects.get(name=data['name']), None
            except related_model.DoesNotExist:
                return None, f'{related_model} does not exists'
        return None, "you need to define mailing's ides or names"

    @classmethod
    def change_specification(cls, request, pk, related_model, filed: str, method: SPECIFICATION_METHOD) -> Response:
        try:
            mailing = Mailing.objects.get(pk=pk)
        except Mailing.DoesNotExist:
            return Response({'message': 'Mailing does not exists'}, status=status.HTTP_404_NOT_FOUND)
        obj, err = cls.get_related_object(request.data, related_model)
        if err:
            return Response({'message': err}, status=status.HTTP_400_BAD_REQUEST)
        sub_object = getattr(mailing, filed)
        getattr(sub_object, method)(obj)
        return Response(status=status.HTTP_200_OK)

    @action(['POST'], detail=True)
    def operator_add(self, request, pk):
        return self.change_specification(request, pk, Operator, 'operators', 'add')

    @action(['POST'], detail=True)
    def operator_remove(self, request, pk):
        return self.change_specification(request, pk, Operator, 'operators', 'remove')

    @action(['POST'], detail=True)
    def tag_add(self, request, pk):
        return self.change_specification(request, pk, Tag, 'tags', 'add')

    @action(['POST'], detail=True)
    def tag_remove(self, request, pk):
        return self.change_specification(request, pk, Tag, 'tags', 'remove')


MessagesView = create_view('Message')

OperatorsView = create_view('Operator')

TagsView = create_view('Tag')

ClientsView = create_view('Client')


class TimezonesView(ViewSet):
    permission_classes = (IsActiveUser,)

    @action(['GET'], detail=False,     permission_classes = (IsActiveUser,))
    def total(self, request):
        """Возвращает список возможных timezone"""
        return Response(pytz.all_timezones, status.HTTP_200_OK)
