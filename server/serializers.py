from datetime import datetime, time
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer
from server.models import *


def create_model_serializer(model_name: str) -> type:
    """Метакласс создающий сериалайзеры по имени модели"""
    meta = type('Meta', (object,), {'fields': '__all__', 'model': globals()[model_name]})
    args = {'Meta': meta}
    return type(f'{model_name}Serializer', (ModelSerializer,), args)


ClientSerializer = create_model_serializer('Client')

TagSerializer = create_model_serializer('Tag')

OperatorSerializer = create_model_serializer('Operator')


class MailingSerializer(create_model_serializer('Mailing')):
    error_message = '"%s" field must be lower then "%s" field'

    @classmethod
    def _start_lower_then_stop_validate(cls, start: datetime, stop: datetime):
        if start >= stop:
            raise ValidationError(cls.error_message % ('start', 'stop'))

    @classmethod
    def _send_time_validate(cls, send_after: time, send_until: time):
        if send_after and send_until and send_after >= send_until:
            raise ValidationError(cls.error_message % ('send_after', 'send_until'))

    def create(self, validated_data):
        self._start_lower_then_stop_validate(validated_data['start'], validated_data['stop'])
        self._send_time_validate(validated_data.get('send_after'), validated_data.get('send_until'))
        return Mailing.objects.create(**validated_data)

    def update(self, instance, validated_data):
        for key in self.get_fields():
            current_value = getattr(instance, key)
            new_value = validated_data.get(key, current_value)
            if key in ('tags', 'operators'):
                continue
            setattr(instance, key, new_value)

        self._start_lower_then_stop_validate(instance.start, instance.stop)
        self._send_time_validate(instance.send_after, instance.send_until)
        instance.save()
        return instance


MessageSerializer = create_model_serializer('Message')
