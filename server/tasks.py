from datetime import timedelta
import requests
from django.utils.datetime_safe import datetime
from rest_framework import status
from server.db import db
from server.models import Mailing, Message
from sms_mailing.celery import app
from sms_mailing.settings import SMS_TOKEN

first_executing = True


def reset_mailing_statuses():
    for mailing in Mailing.objects.filter(status=Mailing.StatusChoice.MAILING):
        mailing.status = Mailing.StatusChoice.CREATED
        mailing.save()


def mailing_time_expired(mailing: Mailing) -> bool:
    """Проверка истекло ли время до которого можно отправлять сообщение"""
    expired_time = mailing.stop + timedelta(hours=14)  # добавляем 14 часов, т.к. +14 последний часовой пояс
    return datetime.now() >= expired_time


def right_time(mailing: Mailing, client_time: datetime) -> bool:
    """Проверяет можно ли отправлять сообщение пользователю в данное время. С учетом всех временных полей Mailing"""
    if mailing.start >= client_time or client_time > mailing.stop:
        return False
    response = True
    if mailing.send_after:
        response = mailing.send_after <= client_time.time()
        if mailing.send_until:
            response = mailing.send_until > client_time.time()
    elif mailing.send_until:
        response = mailing.send_until > client_time.time()
    return response


@app.task
def find_mailings():
    """Выполняется раз в минуту. Ищет рассылки со статусом CREATED запускает их. При запуске Celery, статус всех
    невыполненных рассылок устанавливается как CREATED. После запуска рассылки, статус будет изменен на MAILING
    или COMPLETED в случае завершения"""
    mailings = Mailing.objects.filter(status=Mailing.StatusChoice.CREATED)
    for mailing in mailings:
        if mailing_time_expired(mailing):
            mailing.status = mailing.StatusChoice.COMPLETED
        else:
            mailing.status = mailing.StatusChoice.MAILING
        mailing.save()
        if mailing.status == mailings.StatusChoice.MAILING:
            start_mailing.delay(mailing.id)


def get_clients(mailing):
    """Возвращает кортеж вида Client: (id, phone, timezone)"""
    sql = f"""
    SELECT id, phone, timezone FROM clients
    WHERE id NOT IN (SELECT id FROM messages WHERE mailing_id={mailing.id} AND status={Message.StatusChoice.SENT})
    AND tag_id={mailing.tag.id} AND operator_id={mailing.operator.id}
    """
    return db(sql)


@app.task
def start_mailing(mailing_id: int):
    """Запускает процесс рассылки. С учётом timezone клиента"""
    mailing = Mailing.objects.get(id=mailing_id)
    clients = get_clients(mailing)

    for client_id, client_phone, client_tz in clients:
        client_time = datetime.now(tz=client_tz)
        if right_time(mailing, client_time):
            send_message(mailing, client_id, client_phone)


def send_message(mailing: Mailing, client_id: int, client_phone: str):
    """Отправляет сообщение клиенту"""
    try:
        message = Message.objects.get(client_id=client_id, mailing=mailing)
    except Message.DoesNotExist:
        message = Message.objects.create(client_id=client_id, mailing=mailing)
    url = f'https://probe.fbrq.cloud/v1/send/{message.id}'
    headers = {
        'Content-Type': 'application/json',
        'accept': 'application/json',
        'Authorization': 'Bearer ' + SMS_TOKEN
    }
    data = {
        'id': message.id,
        'phone': client_phone,
        'text': mailing.text
    }
    response = requests.post(url, headers=headers, json=data)

    if response.status_code == status.HTTP_200_OK:
        message.status = Message.StatusChoice.SENT
    else:
        message.status = Message.StatusChoice.ERROR
    message.save()
    return response
