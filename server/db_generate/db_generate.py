from datetime import timedelta, datetime
from random import randint


from server.db_generate.generators import UserGenerator, ModelGenerator, GeneratorField, ClientGenerator, \
    MessageGenerator
from server.models import *


def user_generate() -> [User]:
    """Генерирует пользователей возможного типа"""
    fields = [
        GeneratorField('status', User.StatusChoice.values),
    ]
    generator = UserGenerator(User, fields)
    users = generator.generate()
    print('Users created')
    return users


SYMBOLS = """ 1fFыЫ!"№;%:?*()ХЪ'"<>/?`ё№$-+={}[],.^\\|"""  # набор символов для генерации текстовых полей


def generate_text(symbol: str) -> str:
    """Генерирует текстовое поле случайной длины с обязательным символом"""
    text = []
    for _ in range(randint(3, 32)):
        index = randint(0, len(SYMBOLS) - 1)
        text.append(SYMBOLS[index])
    index = randint(0, len(text) - 1)
    text[index] = symbol
    return ''.join(text)


def mailings_generate() -> [Mailing]:
    """Генерация Mailing, с различным набором Tag, Operator и полем text со случайными символами"""
    now = datetime.now()
    stop = now + timedelta(days=1)
    fields = [
        GeneratorField('start', [now]),
        GeneratorField('stop', [stop]),
        GeneratorField('text', [generate_text(s) for s in SYMBOLS]),
        GeneratorField('status', Mailing.StatusChoice.values),
        GeneratorField('send_after', ['12:00:00']),
        GeneratorField('send_until', ['14:00:00'])
    ]
    generator = ModelGenerator(Mailing, fields)
    mailing = generator.generate()
    print('mailing created')
    return mailing


#  генерация остальных моделей по заданным параметрам
def tags_generate() -> [Tag]:
    fields = [
        GeneratorField('name', [generate_text(s) for s in SYMBOLS]),
    ]
    generator = ModelGenerator(Tag, fields)
    mailing = generator.generate()
    print('tags created')
    return mailing


def operator_generate() -> [Operator]:
    fields = [
        GeneratorField('name', ['megafon', 'mts', 'beeline', 'tele2']),
    ]
    generator = ModelGenerator(Operator, fields)
    operators = generator.generate()
    print('operators created')
    return operators


def client_generate() -> [Client]:
    fields = [
        GeneratorField('operator', Operator.objects.all()),
        GeneratorField('tag', Tag.objects.all()[:10]),
        GeneratorField('timezone', ['America/Adak', 'Etc/UTC', 'Pacific/Kosrae'])
    ]
    generator = ClientGenerator(Client, fields)
    clients = generator.generate()
    print('clients created')
    return clients


def messages_generate() -> [Message]:
    fields = [
        GeneratorField('status', Message.StatusChoice.values),
        GeneratorField('mailing', Mailing.objects.all()[:30]),
    ]

    generator = MessageGenerator(Message, fields)
    clients = generator.generate()
    print('messages created')
    return clients


def db_generate():
    """Создает тестовую ДБ, комбинируя возможные варианты."""
    for name in globals():
        if name.endswith('_generate') and not name.startswith('db_'):
            globals()[name]()
