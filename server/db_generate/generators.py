from copy import deepcopy
from random import randint
from server.models import Client


class GeneratorField:
    """Описание поля для генерации моделей name - имя поля, diapason - возможные варианты"""

    def __init__(self, name: str, diapason: []):
        self.name = name
        self.diapason = diapason


class ModelGenerator:
    """Генерирует объекты для тестовой БД"""

    def __init__(self, model, fields: [GeneratorField]):
        self.variants = None
        self.model = model
        self.fields = fields

    def combine(self, fields: [GeneratorField], instances: [] = None) -> [{}]:
        """Рекурсивная функция. Комбинирует параметры, возвращает все возможные комбинации переданных полей"""
        if not fields:  # крайний случай рекурсии
            self.variants = instances
            return
        field = fields.pop()  # выкидываем поле из массива
        extended = []  # здесь будем сохранять расширенные экземпляры

        if not instances:  # инициализируем вариантами из первого диапазона значений
            for variant in field.diapason:
                extended.append({field.name: variant})
        else:  # комбинируем с существующими экземплярами
            for variant in field.diapason:
                for instance in deepcopy(instances):
                    instance[field.name] = variant
                    extended.append(instance)
        return self.combine(fields, extended)

    def create_objects(self):
        """Создаёт объекты используя варианты сгенерированные self.combine"""
        return [
            self.model.objects.create(**fields)
            for fields in self.variants
        ]

    def generate(self):
        """Запускает полный цикл генерации объектов и возвращает их"""
        self.combine(self.fields)
        return self.create_objects()


class UserGenerator(ModelGenerator):
    def create_objects(self):
        return [
            self.model.objects.create(username=f'user{n}@.domain.ru', **variant)
            for n, variant in enumerate(self.variants)
        ]


class ClientGenerator(ModelGenerator):
    @staticmethod
    def generate_phone():
        while True:
            phone = f'79{randint(10 ** 7, 10 ** 8 - 1)}'
            try:
                Client.objects.get(phone=phone)
            except Client.DoesNotExist:
                return phone

    def create_objects(self):

        return [
            self.model.objects.create(phone=self.generate_phone(), **variant)
            for variant in self.variants
        ]


class ClientGetter:
    def __init__(self):
        self.qs = Client.objects.all()
        self.n = -1

    def __call__(self):
        self.n += 1
        if self.n == len(self.qs):
            self.n = 0
        return self.qs[self.n]


class MessageGenerator(ModelGenerator):
    def create_objects(self):
        get_client = ClientGetter()
        return [
            self.model.objects.create(client=get_client(), **variant)
            for n, variant in enumerate(self.variants)
        ]
