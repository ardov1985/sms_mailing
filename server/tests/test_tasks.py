from datetime import timedelta

from django.test import TestCase
from django.utils.datetime_safe import datetime
from rest_framework import status

from server.models import Mailing, Client
from server.tasks import right_time, send_message
from sms_mailing.settings import DATETIME_FORMAT


class TaskTest(TestCase):
    fixtures = ('test.json',)

    def test_right_time(self):
        now = datetime.now()

        kwargs = {'text': 'test', 'start': now + timedelta(days=2), 'stop': now + timedelta(days=4)}
        result = right_time(Mailing.objects.create(**kwargs), now)
        self.assertEqual(result, False)

        kwargs['send_after'] = now.time()
        kwargs['send_until'] = (now + timedelta(hours=1)).time()
        result = right_time(Mailing.objects.create(**kwargs), now)
        self.assertEqual(result, False)

        kwargs['start'] = now
        kwargs['stop'] = now + timedelta(days=1)
        result = right_time(Mailing.objects.create(**kwargs), datetime.now())
        self.assertEqual(result, True)

        kwargs['send_after'] = None
        kwargs['send_until'] = None
        result = right_time(Mailing.objects.create(**kwargs), datetime.now())
        self.assertEqual(result, True)

        kwargs['send_after'] = datetime.strptime('2022-01-01 12:00:00', DATETIME_FORMAT).time()
        kwargs['send_until'] = datetime.strptime('2022-01-01 13:00:00', DATETIME_FORMAT).time()
        kwargs['start'] = datetime.strptime('2022-01-01 12:00:00', DATETIME_FORMAT)
        kwargs['stop'] = datetime.strptime('2022-02-01 12:00:00', DATETIME_FORMAT)
        now = datetime.strptime('2022-01-01 12:30:00', DATETIME_FORMAT)
        result = right_time(Mailing.objects.create(**kwargs), now)
        self.assertEqual(result, True)

        now = datetime.strptime('2022-01-01 13:30:00', DATETIME_FORMAT)
        result = right_time(Mailing.objects.create(**kwargs), now)
        self.assertEqual(result, False)

    def test_send_message(self):
        mailing = Mailing.objects.first()
        client = Client.objects.first()
        response = send_message(mailing, client.id, client.phone)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
