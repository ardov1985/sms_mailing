from datetime import timedelta
from django.utils.datetime_safe import datetime
from rest_framework import status
from server.models import Mailing, Tag, Client, Message, Operator
from server.tests.base.base import BaseViewsTest, TypeOrNone
from server.tests.base.decorators import active_users, no_auth_and_banned_users
from server.tests.base.normal_methods import ActiveUserDeleteMixin, ActiveUserTotalMixin
from server.tests.base.taboo_methods import TabooBannedTotalMixin, TabooNoAuthTotalMixin
from sms_mailing.settings import DATETIME_FORMAT


class MailingTest(BaseViewsTest, TabooBannedTotalMixin, TabooNoAuthTotalMixin, ActiveUserTotalMixin):
    def setUp(self):
        self.uri = 'mailings'
        self.model = Mailing
        self.data_types = {'id': int, 'start': str, 'stop': str, 'text': str, 'status': str,
                           'send_after': TypeOrNone(str), 'send_until': TypeOrNone(str), 'tags': list,
                           'operators': list}

        # параметры необходимые для проверки тестирования начальной и конечной даты
        self.past = (datetime.now() + timedelta(days=-5)).strftime(DATETIME_FORMAT)
        self.now = datetime.now().strftime(DATETIME_FORMAT)
        self.future = (datetime.now() + timedelta(days=1)).strftime(DATETIME_FORMAT)

    @active_users
    def test_create(self, user):
        # проверка на попытку создать рассылку с некорректным временным интервалом start < stop
        response = user.post(self.uri, {'start': self.future, 'stop': self.past, 'text': 'test_message'})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
            many=True
        )

        # проверка на попытку создать рассылку не передав время
        response = user.post(self.uri, {'text': 'test_message'})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
        )

        # проверка на попытку создать рассылку с некорректным форматом datetime
        response = user.post(self.uri, {'start': '323', 'stop': '3434', 'text': 'test_message'})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
        )

        # корректные данные
        response = user.post(self.uri, {'start': self.now, 'stop': self.future, 'text': 'test_message'})
        self.check_response_types(
            response,
            status.HTTP_201_CREATED,
            self.data_types,
        )

        #  корректные данные с временем отправки
        response = user.post(
            self.uri,
            {'start': self.now, 'stop': self.future, 'text': 'test_message', 'send_after': '12:00:00',
             'send_until': '13:00:00'}
        )
        self.check_response_types(
            response,
            status.HTTP_201_CREATED,
            self.data_types,
        )

    @active_users
    def test_update(self, user):
        # корректные данные
        response = user.patch(self.obj_uri, {'message': 'new_message'})
        self.check_response_types(
            response,
            status.HTTP_200_OK,
        )
        # bad start time
        response = user.patch(self.obj_uri, {'start': '1'})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
        )

        # bad start and stop time
        response = user.patch(self.obj_uri, {'start': self.future, 'stop': self.now})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
        )

        # "send_after' can not be more them "send_until"
        response = user.patch(
            self.obj_uri, {'send_after': '16:00:00', 'send_until': '15:00:00'})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
        )

    @active_users
    def test_add_specification(self, user):
        """Группа тестов добавить/удалить тег/оператор для рассылки"""
        for specification in ('Operator', 'Tag'):
            obj = globals()[specification].objects.first()
            for method in ('add', 'remove'):
                uri = self.obj_uri + f'{specification.lower()}_{method}/'
                response = user.post(uri, {'id': obj.id})
                self.check_response_types(
                    response,
                    status.HTTP_200_OK
                )
                response = user.post(uri, {'name': obj.name})
                self.check_response_types(
                    response,
                    status.HTTP_200_OK
                )

                response = user.post(uri, {'id': 10 ** 5})
                self.check_response_types(
                    response,
                    status.HTTP_400_BAD_REQUEST
                )


class TagTest(BaseViewsTest, TabooBannedTotalMixin, TabooNoAuthTotalMixin, ActiveUserTotalMixin):
    def setUp(self):
        self.uri = 'tags'
        self.model = Tag
        self.data_types = {'id': int, 'name': str}

    @active_users
    def test_create(self, user):
        response = user.post(self.uri, {'name': 'new'})
        self.check_response_types(
            response,
            status.HTTP_201_CREATED,
        )

        # попытка создать дубликат
        response = user.post(self.uri, {'name': 'new'})
        self.check_response_types(
            response,
            status.HTTP_400_BAD_REQUEST,
        )


class OperatorTest(TagTest):
    def setUp(self):
        self.uri = 'operators'
        self.model = Operator


class ClientTest(BaseViewsTest, TabooBannedTotalMixin, TabooNoAuthTotalMixin, ActiveUserTotalMixin):
    def setUp(self):
        self.uri = 'clients'
        self.model = Client


class MessageTest(BaseViewsTest, TabooBannedTotalMixin, TabooNoAuthTotalMixin, ActiveUserTotalMixin):
    def setUp(self):
        self.uri = 'messages'
        self.model = Message


class TimezoneTest(BaseViewsTest):
    def setUp(self):
        self.uri = 'timezones'

    @active_users
    def test_get_timezones(self, user):
        response = user.get(self.uri + 'total/')
        self.check_response_types(
            response,
            status.HTTP_200_OK,
        )

    @no_auth_and_banned_users
    def test_get_timezones(self, user):
        response = user.get(self.uri + 'total/')
        self.assertIn(response.status_code, (status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN))


